package pl.homeproject.ultraflashcards.view;

import pl.homeproject.ultraflashcards.R;
import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class WordsOverviewRow extends LinearLayout {

	private Flashcard flashcard;
	private boolean marked = false;
	private Drawable background;
	
	public WordsOverviewRow(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Flashcard getFlashcard() {
		return flashcard;
	}

	public void setFlashcard(Flashcard flashcard) {
		this.flashcard = flashcard;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
	}

	public void restoreBackgroundResource() {
		setMarked(false);
		setBackground(background);
	}

	public void setMarkedRowBackground() {
		setMarked(true);
		background = getBackground();
		setBackgroundResource(R.drawable.text_view_word_row_overview_boarder);
	}

}
