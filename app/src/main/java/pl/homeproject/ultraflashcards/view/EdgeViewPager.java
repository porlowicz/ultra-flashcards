package pl.homeproject.ultraflashcards.view;

import pl.homeproject.ultraflashcards.R;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class EdgeViewPager extends ViewPager {

	public EdgeViewPager(Context context) {
		super(context);
	}

	public EdgeViewPager(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
	}

	private boolean edgeTouch = false;

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		edgeTouch = false;
		if (ev.getAction() == MotionEvent.ACTION_DOWN && isEdge(ev)) {
			edgeTouch = true;
		}

		if (edgeTouch) {
			return super.onInterceptTouchEvent(ev);
		} else {
			performClick();
			return false;
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent arg0) {
		if (edgeTouch) {
			return super.onTouchEvent(arg0);
		} else {
			performClick();
			return false;
		}
	}

	private boolean isEdge(MotionEvent ev) {
		float marginSize = getResources().getDimension(
				R.dimen.activity_horizontal_margin);
		return ev.getX() < marginSize || ev.getX() > getWidth() - marginSize;
	}

	@Override
	public boolean performClick() {
		return super.performClick();
	}
}
