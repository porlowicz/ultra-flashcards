package pl.homeproject.ultraflashcards.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class MarkAllButton extends Button {

	private boolean markAll = false;
	
	public MarkAllButton(Context context) {
		super(context);
	}

	
	public MarkAllButton(Context context, AttributeSet attrs) {
		super(context, attrs);
	}


	public boolean isMarkAll() {
		return markAll;
	}
	
	public void setMarkAll(boolean markAll) {
		if(markAll){
			setText("Mark All");
		}else{
			setText("Unmark");
		}
		this.markAll = markAll;
	}

}
