package pl.homeproject.ultraflashcards.view.swipe;

public enum SwipeDirection {
	RIGHT(true), LEFT(false);
	
	private boolean state;
	
	private SwipeDirection(boolean b){
		this.state = b;
	}

	public boolean getState() {
		return state;
	}
}
