package pl.homeproject.ultraflashcards.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnimationSet;
import android.widget.TextView;

import pl.homeproject.ultraflashcards.R;
import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import pl.homeproject.ultraflashcards.learning.mode.memory.HalfOfFlashcardPair;
import pl.homeproject.ultraflashcards.learning.mode.memory.MemoryBoardManager;
import pl.homeproject.ultraflashcards.learning.mode.memory.MoveCounterListener;

public class MemoryTextView extends android.support.v7.widget.AppCompatTextView {

    private static final int MARGIN = 2;
    private boolean showed = false;
    private static final String NOT_SHOWED_TEXT = "?";

    private MemoryBoardManager memoryBoardManager;
    private int row;
    private int col;

    public MemoryTextView(Context context, int row, int col) {
        super(context);
        this.memoryBoardManager = MemoryBoardManager.getInstance(context);
        this.row = row;
        this.col = col;
        setRotation(270);
        setGravity(Gravity.CENTER);
        setText(NOT_SHOWED_TEXT);
        setTextColor(ContextCompat.getColor(context, R.color.many_words_font_color));

        setBackground(ContextCompat.getColor(context, R.color.flashcard_background_light));

        HalfOfFlashcardPair cardFor = memoryBoardManager.getCardFor(this);
        if(cardFor.getFlashcard() == null){
            setText(cardFor.getDisplayText());
            setBackground(ContextCompat.getColor(context, R.color.flashcard_background));

            memoryBoardManager.addMoveCounterListener(new MoveCounterListener(this));
        }else {
            setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (!(v instanceof MemoryTextView)) {
                        return;
                    }
                    final MemoryTextView mv = (MemoryTextView) v;

                    if (!mv.showed && !memoryBoardManager.isStatusCard(mv)) {
                        mv.showed = true;

                        ObjectAnimator animation = ObjectAnimator.ofFloat(v, "rotationX", 0.0f, 180f);
                        animation.setInterpolator(new AccelerateDecelerateInterpolator());
                        animation.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                v.setRotationX(0);
                                HalfOfFlashcardPair cardFor = memoryBoardManager.getCardFor(mv);
                                ((TextView) v).setText(cardFor.getDisplayText());
                                setBackground(ContextCompat.getColor(v.getContext(), R.color.flashcard_background));

                                final MemoryBoardManager.MoveState state = memoryBoardManager.select(mv);

                                if (state == MemoryBoardManager.MoveState.CONTINUE) {
                                    mv.showed = true;
                                } else if (state == MemoryBoardManager.MoveState.MATCH) {
                                    setBackground(ContextCompat.getColor(v.getContext(), R.color.memory_green));

                                    MemoryTextView selectedView = memoryBoardManager.getSelectedView();

                                    ObjectAnimator animationFadeOut = createFadeOutAnimation(v);
                                    ObjectAnimator selectedFadeOut = createFadeOutAnimation(selectedView);
                                    animationFadeOut.start();
                                    selectedFadeOut.start();
                                } else if (state == MemoryBoardManager.MoveState.NO_MATCH) {
                                    setBackground(ContextCompat.getColor(v.getContext(), R.color.memory_red));

                                    MemoryTextView selectedView = memoryBoardManager.getSelectedView();

                                    ObjectAnimator animationRestore = createRestoreAnimation(mv);
                                    ObjectAnimator selectedRestore = createRestoreAnimation(selectedView);
                                    animationRestore.start();
                                    selectedRestore.start();
                                }

                                memoryBoardManager.updateCounter();
                            }

                        });

                        animation.start();
                    }

                }

            });
        }
    }

    @NonNull
    private ObjectAnimator createRestoreAnimation(final MemoryTextView mv) {
        ObjectAnimator animationRestore = ObjectAnimator.ofFloat(mv, "rotationX", 0.0f, 180f);
        animationRestore.setInterpolator(new AccelerateDecelerateInterpolator());
        animationRestore.setStartDelay(500);
        animationRestore.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                mv.setRotationX(0);
                mv.setBackground(ContextCompat.getColor(mv.getContext(), R.color.flashcard_background_light));
                mv.setText(NOT_SHOWED_TEXT);
                mv.showed = false;
            }

        });
        return animationRestore;
    }

    @NonNull
    private ObjectAnimator createFadeOutAnimation(View v) {
        ObjectAnimator animationFadeOut = ObjectAnimator.ofFloat(v, "alpha", 0.0f);
        animationFadeOut.setInterpolator(new AccelerateDecelerateInterpolator());
        animationFadeOut.setStartDelay(1000);
        return animationFadeOut;
    }


    public MemoryTextView(Context context) {
        super(context);
        setRotation(270);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        View parent = (View) getParent();
        int measuredWidth = parent.getMeasuredWidth();
        int measuredHeight = MeasureSpec.getSize(heightMeasureSpec);
        int oneWidth = measuredHeight / memoryBoardManager.getBoardWidth() - MARGIN;
        int oneHeight = measuredWidth / memoryBoardManager.getBoardHeight() - MARGIN;

        setMeasuredDimension(oneWidth, oneHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        setTranslationX(alignTranslationX() + getHeight() * row + (row * MARGIN));
        setTranslationY(alignTranslationY() + getWidth() * col + (col * MARGIN));

        super.onDraw(canvas);
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    private float alignTranslationX() {
        return -getPivotX() + getPivotY();
    }

    private float alignTranslationY() {
        return getPivotX() - getPivotY();
    }


    private void setBackground(int color) {
        GradientDrawable border = new GradientDrawable();
        border.setColor(color); //white background
        border.setStroke(1, 0xFF000000);
        setBackground(border);
    }
}
