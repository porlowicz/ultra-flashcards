package pl.homeproject.ultraflashcards.view.swipe;

import java.util.List;

import pl.homeproject.ultraflashcards.wire.UltraFlashcardsViewWire;
import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import pl.homeproject.ultraflashcards.flashcards.FlashcardDealer;
import pl.homeproject.ultraflashcards.view.WordsOverviewRow;
import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class SwipeTextView extends TextView {

	private GestureDetector detector;

	public SwipeTextView(Context context) {
		super(context);
		detector = new GestureDetector(context, new TextViewGestureDetector(this));
	}

	public SwipeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		detector = new GestureDetector(context, new TextViewGestureDetector(this));
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if(detector.onTouchEvent(ev)){
			return true;
		}
		return super.onTouchEvent(ev);
	}

	private static class TextViewGestureDetector implements
			GestureDetector.OnGestureListener {

		private SwipeTextView swipeTextView;

		public TextViewGestureDetector(SwipeTextView swipeTextView) {
			this.swipeTextView = swipeTextView;
		}

		@Override
		public boolean onDown(MotionEvent e) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onShowPress(MotionEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			
			return true;
		}

		@Override
		public void onLongPress(MotionEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			
			ViewGroup parent = findWordsOverviewRowParent(swipeTextView);
			
			if(parent instanceof WordsOverviewRow){
				final WordsOverviewRow overviewRow = (WordsOverviewRow) parent;
				final SwipeDirection direction;
				if(velocityX > 0){
					direction = SwipeDirection.RIGHT;
				}else{
					direction = SwipeDirection.LEFT;
				}
				Animation makeOutAnimation = AnimationUtils.makeOutAnimation(swipeTextView.getContext(), direction.getState());
				makeOutAnimation.setAnimationListener(new AnimationListener() {
					private Flashcard flashcard;
					
					@Override
					public void onAnimationStart(Animation animation) {
						flashcard = overviewRow.getFlashcard();
					}
					
					@Override
					public void onAnimationRepeat(Animation animation) {
					}
					
					@Override
					public void onAnimationEnd(Animation animation) {
						overviewRow.clearAnimation();
						overviewRow.setVisibility(View.INVISIBLE);
						if(flashcard.getId() == -1){
							return;
						}
						FlashcardDealer flashcardDealer = FlashcardDealer.getInstance(swipeTextView.getContext());
						flashcardDealer.move(flashcard, direction);
						
						List<Flashcard> pageContent = flashcardDealer.getCurrentPage();
						ViewGroup containerLayout = (ViewGroup)overviewRow.getParent();
						boolean greenLight = true;
						for(int i = containerLayout.getChildCount()-1; i >= 0; i--){
							WordsOverviewRow row = (WordsOverviewRow) containerLayout.getChildAt(i);
							if(row.getAnimation() != null){
								greenLight = false;
								break;
							}
						}
						if(!greenLight){
							return;
						}
						for(int i = containerLayout.getChildCount()-1; i >= 0; i--){
							WordsOverviewRow row = (WordsOverviewRow) containerLayout.getChildAt(i);
							row.setFlashcard(pageContent.get(i));
							row.setVisibility(View.VISIBLE);
							UltraFlashcardsViewWire.updateLearnTextFields(row);
						}
						
					}
				});
				overviewRow.startAnimation(makeOutAnimation);
				
			}
			
			
			return true;
		}

		private ViewGroup findWordsOverviewRowParent(View view) {
			
			ViewParent parent = view.getParent();
			while(!(parent instanceof WordsOverviewRow)){
				if(parent == null){
					return null;
				}
				parent = parent.getParent();
			}

			return (ViewGroup) parent;
		}

	}
}
