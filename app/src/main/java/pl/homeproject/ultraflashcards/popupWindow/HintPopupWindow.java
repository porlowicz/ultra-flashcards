package pl.homeproject.ultraflashcards.popupWindow;

import pl.homeproject.ultraflashcards.R;
import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class HintPopupWindow extends AbstractPopupWindow{

	private Flashcard flashcard;

	public HintPopupWindow(final Context context, final Flashcard flashcard) {
		super(context, R.layout.hint_popup_window);
		this.flashcard = flashcard;
		
		final TextView hintTextView = (TextView) getLayout().findViewById(R.id.hintTextView);
		hintTextView.setText(flashcard.getHint());
		
		final TextView hintWordFromTextView = (TextView) getLayout().findViewById(R.id.hint_word_from_textView);
		hintWordFromTextView.setText(flashcard.getWordFrom());
		
		View hintMeButton = getLayout().findViewById(R.id.hintMeButton);
		hintMeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				flashcard.moreHint();
				hintTextView.setText(flashcard.getHint());
			}
		});
		
		View hintCloseButton = getLayout().findViewById(R.id.hintCloseButton);
		hintCloseButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				flashcard.clearHint();
				dismiss();
			}
		});
		
	}

	@Override
	public void dismiss() {
		super.dismiss();
		flashcard.clearHint();
	}
	
}
