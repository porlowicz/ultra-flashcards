package pl.homeproject.ultraflashcards.learning.mode.memory;


import pl.homeproject.ultraflashcards.flashcards.Flashcard;

public class HalfOfFlashcardPair {

    private final Flashcard flashcard;
    private final String displayText;

    public HalfOfFlashcardPair(Flashcard flashcard, String displayText){
        this.flashcard = flashcard;
        this.displayText = displayText;
    }

    public Flashcard getFlashcard() {
        return flashcard;
    }

    public String getDisplayText() {
        return displayText;
    }
}
