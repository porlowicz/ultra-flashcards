package pl.homeproject.ultraflashcards.learning.mode.memory;

import pl.homeproject.ultraflashcards.view.MemoryTextView;

public class MoveCounterListener {

    private MemoryTextView memoryTextView;

    public MoveCounterListener(MemoryTextView memoryTextView){

        this.memoryTextView = memoryTextView;
    }

    public void updateStatusCard(int counter){

        memoryTextView.setText("Moves: " + counter);

    }
}
