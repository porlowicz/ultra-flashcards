package pl.homeproject.ultraflashcards.learning.mode.memory;


import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import pl.homeproject.ultraflashcards.flashcards.FlashcardDealer;
import pl.homeproject.ultraflashcards.view.MemoryTextView;

public class MemoryBoardManager {

    private static MemoryBoardManager INSTANCE;

    private HalfOfFlashcardPair selectedCard;
    private MemoryTextView selectedView;
    private List<HalfOfFlashcardPair> currentBoard;
    private int movesCounter = 0;
    private MoveCounterListener moveCounterListener;

    private int flashcardOffset = 0;

    public void addMoveCounterListener(MoveCounterListener moveCounterListener) {
        this.moveCounterListener = moveCounterListener;
    }

    public void previousOffset() {
        flashcardOffset -= getBoardSize();
        if (flashcardOffset < 0) {
            flashcardOffset = 0;
        }
    }

    public void nextOffset() {
        if(flashcardOffset > flashcardDealer.getAllFlashcards().size()){
            return;
        }
        flashcardOffset += getBoardSize();
    }

    public int getBoardSize() {
        return (boardWidth * boardHeight) / 2;
    }

    public enum MoveState {
        CONTINUE, NO_MATCH, MATCH, STATUS_CARD;
    }

    private final FlashcardDealer flashcardDealer;
    // in "fake" landspace orientation
    private int boardWidth = 3;
    private int boardHeight = 5;

    private MemoryBoardManager(Context context) {
        this.flashcardDealer = FlashcardDealer.getInstance(context);
    }

    public static MemoryBoardManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new MemoryBoardManager(context);
        }
        return INSTANCE;
    }

    public HalfOfFlashcardPair getCardFor(MemoryTextView mv) {
        return currentBoard.get(boardWidth * mv.getRow() + mv.getCol());
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public boolean prepareBoard() {
        int boardSize = getBoardSize();

        currentBoard = new ArrayList<>(boardSize * 2);
        movesCounter = 0;
        selectedCard = null;

        List<Flashcard> flashcards = flashcardDealer.getFlashcards(flashcardOffset, boardSize);

        if(flashcards.isEmpty()){
            return false;
        }

        for(int i = flashcards.size(); i < boardSize; i++){
            flashcards.add(Flashcard.create("", ""));
        }

        for (Flashcard f : flashcards) {
            currentBoard.add(new HalfOfFlashcardPair(f, f.getWordFrom()));
            currentBoard.add(new HalfOfFlashcardPair(f, f.getWordTo()));
        }

        Random r = new Random(System.currentTimeMillis());
        for (int i = 0; i < boardSize * 4; i++) {
            int indexFrom = r.nextInt(boardSize * 2);
            int indexTo = r.nextInt(boardSize * 2);
            HalfOfFlashcardPair removed = currentBoard.remove(indexFrom);
            currentBoard.add(indexTo, removed);
        }

        int statusCardIndex = r.nextInt(boardSize * 2);
        currentBoard.add(statusCardIndex, new StatusCard(null, "Moves: " + movesCounter));
        return true;
    }

    public boolean isStatusCard(MemoryTextView mv) {
        return currentBoard.get(boardWidth * mv.getRow() + mv.getCol()).getFlashcard() == null;
    }

    public MoveState select(MemoryTextView mv) {
        HalfOfFlashcardPair currentSelect = currentBoard.get(boardWidth * mv.getRow() + mv.getCol());

        if (selectedCard == null) {
            selectedCard = currentSelect;
            selectedView = mv;
            return MoveState.CONTINUE;
        }

        if (currentSelect == selectedCard) {
            return MoveState.CONTINUE;
        }

        if (currentSelect.getFlashcard() == null) {
            return MoveState.STATUS_CARD;
        } else if (currentSelect.getFlashcard().getId() == selectedCard.getFlashcard().getId()) {
            selectedCard = null;
            movesCounter++;
            return MoveState.MATCH;
        }
        movesCounter++;
        selectedCard = null;
        return MoveState.NO_MATCH;
    }

    public MemoryTextView getSelectedView() {
        return selectedView;
    }

    public void updateCounter() {
        if (moveCounterListener != null) {
            moveCounterListener.updateStatusCard(movesCounter);
        }
    }
}
