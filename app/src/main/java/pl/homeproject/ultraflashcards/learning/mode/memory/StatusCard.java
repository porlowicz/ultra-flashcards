package pl.homeproject.ultraflashcards.learning.mode.memory;

import pl.homeproject.ultraflashcards.flashcards.Flashcard;

/**
 * Created by Lucyna on 2017-08-27.
 */

public class StatusCard extends HalfOfFlashcardPair {

    public StatusCard(Flashcard flashcard, String displayText) {
        super(flashcard, displayText);
    }
}
