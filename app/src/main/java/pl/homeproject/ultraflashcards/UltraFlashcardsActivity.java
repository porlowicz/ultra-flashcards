package pl.homeproject.ultraflashcards;

import java.util.Locale;

import pl.homeproject.ultraflashcards.learning.mode.memory.MemoryBoardManager;
import pl.homeproject.ultraflashcards.view.MemoryTextView;
import pl.homeproject.ultraflashcards.wire.MemoryBoardViewWire;
import pl.homeproject.ultraflashcards.wire.SingleWordBoardViewWire;
import pl.homeproject.ultraflashcards.wire.UltraFlashcardsViewWire;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UltraFlashcardsActivity extends FragmentActivity {

    private enum ReplacableFragment {
        SINGLE_WORD_FRAGMENT, MANY_WORD_FRAGMENT;
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
     * derivative, which will keep every loaded fragment in memory. If this
     * becomes too memory intensive, it may be best to switch to a
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    private UltraFlashcardsViewWire viewWire;
    private SingleWordBoardViewWire singleWordBoardViewWire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ultra_flashcards);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(3);

        singleWordBoardViewWire = new SingleWordBoardViewWire(this);
        viewWire = new UltraFlashcardsViewWire(this);

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int arg0) {
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mViewPager.getWindowToken(), 0);
                singleWordBoardViewWire.fillSingleWordLearningBoard(
                        UltraFlashcardsActivity.this.findViewById(R.id.fragment_single_word));
                viewWire.fillLearningBoard(UltraFlashcardsActivity.this.findViewById(R.id.fragment_1));
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ultra_flashcards, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings_word_list) {
            Intent intent = new Intent(this, MyWordsManagerActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class
            // below).
            return PlaceholderFragment.newInstance(position, UltraFlashcardsActivity.this);
        }

        @Override
        public int getCount() {
            // Show n total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return "";
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {

        }

        public PlaceholderFragment() {
        }

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private int sectionNumber;
        private UltraFlashcardsViewWire viewWire;
        private SingleWordBoardViewWire singleWordBoardViewWire;
        private MemoryBoardViewWire memoryBoardViewWire;

        /**
         * Returns a new instance of this fragment for the given section number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber, Context context) {
            PlaceholderFragment fragment = PlaceholderFragment.build(sectionNumber, context);
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public static PlaceholderFragment build(int sectionNumber, Context context) {
            PlaceholderFragment pf = new PlaceholderFragment();
            pf.sectionNumber = sectionNumber;
            pf.viewWire = new UltraFlashcardsViewWire(context);
            pf.singleWordBoardViewWire = new SingleWordBoardViewWire(context);
            pf.memoryBoardViewWire = new MemoryBoardViewWire(context);
            return pf;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView;
            switch (sectionNumber) {
                case 0:

                    rootView = inflater.inflate(
                            R.layout.fragment_memory, container, false);
                    memoryBoardViewWire.initBoard(rootView);
                    memoryBoardViewWire.wireRestart(rootView);
                    memoryBoardViewWire.wireNext(rootView);
                    memoryBoardViewWire.wirePrevious(rootView);
                    break;

                case 1:
                    rootView = inflater.inflate(
                            R.layout.fragment_single_word, container, false);
                    singleWordBoardViewWire.fillSingleWordLearningBoard(rootView);
                    break;
                default:
                    rootView = inflater.inflate(
                            R.layout.fragment_ultra_flashcards_1, container, false);
                    viewWire.wirePagingButtons(rootView);
                    viewWire.fillLearningBoard(rootView, 0);
                    break;
            }

            return rootView;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewWire.fillLearningBoard(findViewById(R.id.fragment_1));
    }
}
