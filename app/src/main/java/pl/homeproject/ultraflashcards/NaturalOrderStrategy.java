package pl.homeproject.ultraflashcards;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.homeproject.ultraflashcards.flashcards.Flashcard;

import com.google.common.collect.Lists;

public class NaturalOrderStrategy extends OrderStrategy{

	@Override
	public List<Flashcard> transform(final List<Flashcard> flashcards) {
		List<Flashcard> result = Lists.newArrayList(flashcards);
		
		Collections.sort(result, new Comparator<Flashcard>() {

			@Override
			public int compare(Flashcard lhs, Flashcard rhs) {
				return lhs.getSort() - rhs.getSort();
			}
		});
		return result;
	}

}
