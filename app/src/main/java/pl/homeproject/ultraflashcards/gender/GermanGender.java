package pl.homeproject.ultraflashcards.gender;



public class GermanGender {

	private static final String MASCULINE = "der";
	private static final String FEMININE = "die";
	private static final String NEUTER = "das";

	public static Gender get(String word) {
		String token = word.trim().toLowerCase();
		if(MASCULINE.equals(token)){
			return Gender.MASCULINE;
		}
		if(FEMININE.equals(token)){
			return Gender.FEMININE;
		}
		if(NEUTER.equals(token)){
			return Gender.NEUTER;
		}
		return Gender.NOT_DEFINED;
	}

	public static String strip(String word) {
		if(word.startsWith(MASCULINE + " ") || word.startsWith(FEMININE + " ") || word.startsWith(NEUTER + " ")){
			return word.substring(4);
		}
		return word;
	}
}
