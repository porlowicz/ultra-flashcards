package pl.homeproject.ultraflashcards.gender;

import pl.homeproject.ultraflashcards.R;

public enum Gender {

	MASCULINE(R.color.masculine), FEMININE(R.color.feminine), NEUTER(
			R.color.neuter), NOT_DEFINED(R.color.flashcard_background);

	private final int colorId;

	private Gender(int colorResourceId) {
		this.colorId = colorResourceId;
	}

	public int getColorResourceId() {
		return colorId;
	}

}
