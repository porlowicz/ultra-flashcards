package pl.homeproject.ultraflashcards.flashcards;

import java.util.Random;

import pl.homeproject.ultraflashcards.gender.Gender;
import pl.homeproject.ultraflashcards.gender.GermanGender;

public class Flashcard {

    public static final int UNDEFINED_ID = -1;

    public static Flashcard EMPTY = new Flashcard(UNDEFINED_ID, "", "",
            Integer.MAX_VALUE, 0, 0, "");
    private int id = UNDEFINED_ID;
    private String wordFrom;
    private String wordTo;
    private int hintState = 3;
    private int rightSwipeCounter = 0;
    private int leftSwipeCounter = 0;
    private String cjkIdeograph = "";

    private int sort;

    private static final Random r = new Random();

    private Flashcard(int id2, String wordFrom, String wordTo, int sort, int rightSwipe, int leftSwipe, String cjkIdeograph) {
        id = id2;
        this.wordFrom = wordFrom;
        this.wordTo = wordTo;
        this.sort = sort;
        this.rightSwipeCounter = rightSwipe;
        this.leftSwipeCounter = leftSwipe;
        this.cjkIdeograph = cjkIdeograph;
    }

    public static Flashcard create(String wordFrom, String wordTo) {
        return new Flashcard(UNDEFINED_ID, wordFrom, wordTo, Integer.MAX_VALUE, 0, 0, "");
    }

    public static Flashcard create(int id, String wordFrom, String wordTo,
                                   int sort, int rightSwipe, int leftSwipe, String cjkIdeograph) {
        return new Flashcard(id, wordFrom, wordTo, sort, rightSwipe, leftSwipe, cjkIdeograph);
    }

    public String getWordFrom() {
        return wordFrom;
    }

    public String getWordTo() {
        return wordTo;
    }

    public void setWordFrom(String wordFrom) {
        this.wordFrom = wordFrom;
    }

    public void setWordTo(String wordTo) {
        this.wordTo = wordTo;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public boolean updateSort(int i) {
        if (i != sort) {
            sort = i;
            return true;
        }
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWordFromBackgroundColor() {
        return getGenderBackgroundColorResourceId(this.wordFrom);
    }

    public int getWordToBackgroundColor() {
        return getGenderBackgroundColorResourceId(this.wordTo);
    }

    private static int getGenderBackgroundColorResourceId(String word) {
        String[] tokens = word.split("\\s");
        if (tokens.length == 2) {
            Gender gender = GermanGender.get(tokens[0]);
            return gender.getColorResourceId();
        }
        return Gender.NOT_DEFINED.getColorResourceId();
    }

    public String getHint() {

        String result = "";
        for (int i = 0; i < wordTo.length(); i++) {
            String letter = String.valueOf(wordTo.charAt(i));
            if (i < hintState) {
                result += letter;
            } else {
                if (" ".equals(letter)) {
                    result += letter;
                } else {
                    result += "_";
                }
            }
        }

        return result;
    }

    public void moreHint() {
        if (hintState < wordTo.length() / 2) {
            hintState += 2;
        } else {
            hintState += 4;
        }
    }

    public String getCjkIdeograph() {
        return cjkIdeograph;
    }

    public void setCjkIdeograph(String cjkIdeograph) {
        this.cjkIdeograph = cjkIdeograph;
    }

    public void clearHint() {
        hintState = 3;
    }

    public void rightSwipe() {
        rightSwipeCounter++;
    }

    public void leftSwipe() {
        leftSwipeCounter++;
    }

    public int getRightSwipeOffset() {
        return rightSwipeCounter;
    }

    public int getLeftSwipeOffset() {
        return leftSwipeCounter;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Flashcard)) {
            return false;
        }

        Flashcard f = (Flashcard) o;
        if (id != UNDEFINED_ID && f.getId() == id) {
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Integer.valueOf(id).hashCode();
    }
}
