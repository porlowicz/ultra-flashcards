package pl.homeproject.ultraflashcards.flashcards;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import pl.homeproject.ultraflashcards.flashcards.storage.FlashcardStore;

import android.content.Context;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.zxing.common.CharacterSetECI;

public class FlashcardProvider {

    Executor executor = Executors.newFixedThreadPool(1);

    private final FlashcardStore flashcardStore;

    private Set<String> uniqueCjk = Sets.newHashSet();
    private static final int cjkFrom = 0x4E00;
    private static final int cjkTo = 0x9FFF;
    private static final int cjkRange = cjkTo - cjkFrom;

    private List<Flashcard> allFlashcards = Lists.newArrayList();

    private FlashcardProvider(Context context) {
        this.flashcardStore = FlashcardStore.getInstance(context);
        loadAllFlashcards();
    }

    private static FlashcardProvider FLASHCARD_PROVIDER;

    public static FlashcardProvider getInstance(Context context) {
        if (FLASHCARD_PROVIDER == null) {
            FLASHCARD_PROVIDER = new FlashcardProvider(context);
        }
        return FLASHCARD_PROVIDER;
    }

    private void loadAllFlashcards() {
        List<Flashcard> flashcards = flashcardStore.findAllFlashcards();

        allFlashcards.clear();
        if (flashcards.isEmpty()) {
            return;
        }

        for(Flashcard f : flashcards){
            uniqueCjk.add(f.getCjkIdeograph());
        }
        allFlashcards = Lists.newArrayList(flashcards);
    }

    public void save(final List<Flashcard> data) {

        Iterable<Flashcard> newCards = Iterables.filter(data, new Predicate<Flashcard>() {
            @Override
            public boolean apply(Flashcard flashcard) {
                return flashcard.getId() == Flashcard.UNDEFINED_ID;
            }
        });

        List<Flashcard> newCardsList = Lists.newArrayList(newCards);
        flashcardStore.save(newCardsList);

        if (newCardsList.size() != data.size()) {
            Iterable<Flashcard> updateCards = Iterables.filter(data, new Predicate<Flashcard>() {
                @Override
                public boolean apply(Flashcard flashcard) {
                    return flashcard.getId() != Flashcard.UNDEFINED_ID;
                }
            });
            update(Sets.newHashSet(updateCards));
        }

    }

    public void update(final Set<Flashcard> data) {
        flashcardStore.update(data);
    }

    public List<Flashcard> getAllFlashcards() {
        return allFlashcards;
    }

    public List<Flashcard> getPage(int offset, int limit) {
        List<Flashcard> result = new ArrayList<>(limit);
        if (offset < allFlashcards.size()) {
            int index = offset;
            while (index < offset + limit && index < allFlashcards.size()) {
                result.add(allFlashcards.get(index));
                index++;
            }
            return result;
        } else {
            return result;
        }
    }

    public void remove(List<Flashcard> flashcards) {
        flashcardStore.remove(flashcards);
        for (Flashcard f : flashcards) {
            uniqueCjk.remove(f.getCjkIdeograph());
        }
    }

    public Set<Flashcard> fixSortOrder(List<Flashcard> allFlashcards) {
        int i = 0;
        Set<Flashcard> updated = new HashSet<>();
        for(Flashcard f : allFlashcards){
            f.setSort(i++);
            updated.add(f);
        }
        return updated;
    }


    public String generateUniqueCjk(final String wordFrom, final String wordTo) {

        int charCode = (Math.abs(wordFrom.hashCode() + wordTo.hashCode())) % cjkRange;
        int safeCounter = cjkRange;
        while (uniqueCjk.contains(Character.toString((char) (cjkFrom + charCode))) && safeCounter > 0) {
            charCode = (charCode + 1) % cjkRange;
            safeCounter--;
        }
        String cjk = Character.toString((char) (cjkFrom + charCode));
        uniqueCjk.add(cjk);
        return cjk;
    }

    public boolean hasElements(int i) {
        return allFlashcards.size() > i;
    }
}
