package pl.homeproject.ultraflashcards.flashcards.storage;

import java.util.List;
import java.util.Set;

import pl.homeproject.ultraflashcards.flashcards.Flashcard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.common.collect.Lists;

public class FlashcardStore extends SQLiteOpenHelper {

    private static FlashcardStore flashcardStore;

    private static final String DB_NAME = "ULTRA_FLASHCARDS_DB";
    private static final int DB_VERSION = 5;

    private static final String CREATE_TABLE_FLASHCARD = "CREATE TABLE flashcard ("
            + "f_id INTEGER PRIMARY KEY, "
            + "f_word_from TEXT NOT NULL,"
            + "f_word_to TEXT NOT NULL)";

    private static final String ADD_COLUMN_SORT_FLASHCARD = "ALTER TABLE flashcard ADD COLUMN "
            + "f_sort INTEGER DEFAULT 2147483647";

    private static final String ADD_COLUMN_RIGHT_SWIPE_FLASHCARD = "ALTER TABLE flashcard ADD COLUMN "
            + "f_right_swipe_offset INTEGER DEFAULT 0";

    private static final String ADD_COLUMN_LEFT_SWIPE_FLASHCARD = "ALTER TABLE flashcard ADD COLUMN "
            + "f_left_swipe_offset INTEGER DEFAULT 0";

    private static final String ADD_COLUMN_CJK_IDEOGRAPH = "ALTER TABLE flashcard ADD COLUMN "
            + "f_cjk_ideograph TEXT DEFAULT ''";


    public static FlashcardStore getInstance(Context context) {
        if (flashcardStore == null) {
            flashcardStore = new FlashcardStore(context);
        }
        return flashcardStore;
    }

    private FlashcardStore(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        getWritableDatabase().close();
    }

    public FlashcardStore(Context context, String name, CursorFactory factory,
                          int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FLASHCARD);
        db.execSQL(ADD_COLUMN_SORT_FLASHCARD);
        db.execSQL(ADD_COLUMN_RIGHT_SWIPE_FLASHCARD);
        db.execSQL(ADD_COLUMN_LEFT_SWIPE_FLASHCARD);
        db.execSQL(ADD_COLUMN_CJK_IDEOGRAPH);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion == 1) {
            db.execSQL(ADD_COLUMN_SORT_FLASHCARD);
        }
        if (oldVersion == 3) {
            db.execSQL(ADD_COLUMN_RIGHT_SWIPE_FLASHCARD);
            db.execSQL(ADD_COLUMN_LEFT_SWIPE_FLASHCARD);
        }
        if (oldVersion == 4) {
            db.execSQL(ADD_COLUMN_CJK_IDEOGRAPH);
        }

    }

    public void updateRandomSeed(int seedOffset) {

    }

    public List<Flashcard> findAllFlashcards() {

        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.query("flashcard",
                new String[]{
                        "f_id",
                        "f_word_from",
                        "f_word_to",
                        "f_sort",
                        "f_right_swipe_offset",
                        "f_left_swipe_offset",
                        "f_cjk_ideograph"},
                null,
                null,
                null,
                null,
                "f_sort asc");


        List<Flashcard> result = Lists.newArrayList();
        while (c.moveToNext()) {
            int id = c.getInt(c.getColumnIndex("f_id"));
            String wordFrom = c.getString(c.getColumnIndex("f_word_from"));
            String wordTo = c.getString(c.getColumnIndex("f_word_to"));
            int sort = c.getInt(c.getColumnIndex("f_sort"));
            int rightSwipe = c.getInt(c.getColumnIndex("f_right_swipe_offset"));
            int leftSwipe = c.getInt(c.getColumnIndex("f_left_swipe_offset"));
            String cjk = c.getString(c.getColumnIndex("f_cjk_ideograph"));
            Flashcard flashcard = Flashcard.create(id, wordFrom, wordTo, sort, rightSwipe, leftSwipe, cjk);
            result.add(flashcard);
        }

        c.close();

        return result;
    }

    public void update(Set<Flashcard> update) {
        if (update.isEmpty()) {
            return;
        }
        SQLiteDatabase db = getWritableDatabase();

        for (Flashcard flashcard : update) {
            ContentValues values = new ContentValues();
            values.put("f_word_from", flashcard.getWordFrom());
            values.put("f_word_to", flashcard.getWordTo());
            values.put("f_sort", flashcard.getSort());
            values.put("f_right_swipe_offset", flashcard.getRightSwipeOffset());
            values.put("f_left_swipe_offset", flashcard.getLeftSwipeOffset());
            values.put("f_cjk_ideograph", flashcard.getCjkIdeograph());

            db.update("flashcard", values, "f_id = ?", new String[]{"" + flashcard.getId()});
        }
    }

    public void save(List<Flashcard> save) {
        if (save.isEmpty()) {
            return;
        }
        SQLiteDatabase db = getWritableDatabase();

        for (Flashcard flashcard : save) {
            ContentValues values = new ContentValues();
            values.put("f_word_from", flashcard.getWordFrom());
            values.put("f_word_to", flashcard.getWordTo());
            values.put("f_sort", flashcard.getSort());
            values.put("f_right_swipe_offset", flashcard.getRightSwipeOffset());
            values.put("f_left_swipe_offset", flashcard.getLeftSwipeOffset());
            values.put("f_cjk_ideograph", flashcard.getCjkIdeograph());

            db.insert("flashcard", null, values);

            flashcard.setId(findFlashcardIdFor(db, flashcard.getWordFrom(), flashcard.getWordTo()));
        }

    }


    private int findFlashcardIdFor(SQLiteDatabase db, String wordFrom, String wordTo) {

        Cursor c = db.query("flashcard",
                new String[]{
                        "f_id",
                },
                "f_word_from = ? AND f_word_to = ?",
                new String[]{wordFrom, wordTo},
                null,
                null,
                null);

        int id = -1;
        if (c.moveToNext()) {
            id = c.getInt(c.getColumnIndex("f_id"));
        }

        c.close();

        return id;
    }

    public void remove(List<Flashcard> flashcards) {

        SQLiteDatabase db = getWritableDatabase();

        for (Flashcard f : flashcards) {
            db.delete("flashcard", "f_id = ?", new String[]{"" + f.getId()});
        }

    }

}
