package pl.homeproject.ultraflashcards.flashcards;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import pl.homeproject.ultraflashcards.gender.GermanGender;
import pl.homeproject.ultraflashcards.view.swipe.SwipeDirection;

import android.content.Context;

import com.google.common.collect.Lists;

public class FlashcardDealer {

    private static FlashcardDealer FLASHCARD_DEALER;
    private final FlashcardProvider flashcardProvider;

    private static final int PAGE_SIZE = 5;
    private int page = 0;

    private static final int RIGHT_SWIPE_BASE = PAGE_SIZE * 2;
    private static final int LEFT_SWIPE_BASE = PAGE_SIZE;

    private Stack<Flashcard> movedCardsHistory = new Stack<>();

    private FlashcardDealer(Context context) {
        this.flashcardProvider = FlashcardProvider.getInstance(context);
    }

    public static FlashcardDealer getInstance(Context context) {
        if (FLASHCARD_DEALER == null) {
            FLASHCARD_DEALER = new FlashcardDealer(context);
        }
        return FLASHCARD_DEALER;
    }

    public void update(Set<Flashcard> data) {
        flashcardProvider.update(data);
    }

    public List<Flashcard> getAllFlashcards() {
        return flashcardProvider.getAllFlashcards();
    }

    public List<Flashcard> getPreviousPage() {
        if (page > 0) {
            page--;
            return getPage(page);
        }
        return Lists.newArrayList();
    }

    public List<Flashcard> getNextPage() {
        if (flashcardProvider.hasElements((page + 1) * PAGE_SIZE)) {
            page++;
        }

        return getPage(page);
    }

    public List<Flashcard> getCurrentPage() {
        return getPage(page);
    }

    public int getPaginationCurrent() {
        return page + 1;
    }

    public int getPaginationMax() {
        if (flashcardProvider.getAllFlashcards().isEmpty()) {
            return 0;
        }
        return (flashcardProvider.getAllFlashcards().size() - 1) / PAGE_SIZE + 1;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void remove(List<Flashcard> flashcards) {
        List<Flashcard> allFlashcards = flashcardProvider.getAllFlashcards();
        allFlashcards.removeAll(flashcards);
        Set<Flashcard> modified = flashcardProvider.fixSortOrder(allFlashcards);
        flashcardProvider.remove(flashcards);
        flashcardProvider.update(modified);
    }

    public List<Flashcard> getPage(int page) {
        final List<Flashcard> pageContent = flashcardProvider.getPage(page
                * PAGE_SIZE, PAGE_SIZE);
        if (pageContent.size() < PAGE_SIZE) {
            for (int i = pageContent.size() - 1; i < PAGE_SIZE; i++) {
                pageContent.add(Flashcard.EMPTY);
            }
        }
        return pageContent;
    }

    public void move(Flashcard flashcard, SwipeDirection direction) {
        movedCardsHistory.push(flashcard);

        final int offset;
        if (direction == SwipeDirection.RIGHT) {
            flashcard.rightSwipe();
            offset = RIGHT_SWIPE_BASE + flashcard.getRightSwipeOffset();
        } else {
            flashcard.leftSwipe();
            offset = LEFT_SWIPE_BASE
                    + (flashcard.getLeftSwipeOffset() % PAGE_SIZE);
        }
        List<Flashcard> allFlashcards = flashcardProvider.getAllFlashcards();
        allFlashcards.remove(flashcard);
        if (offset < allFlashcards.size()) {
            allFlashcards.add(offset, flashcard);
        } else {
            allFlashcards.add(flashcard);
        }
        Set<Flashcard> modified = flashcardProvider.fixSortOrder(allFlashcards);
        flashcardProvider.update(modified);
    }

    public void undo() {
        if (movedCardsHistory.isEmpty()) {
            return;
        }

        Flashcard flashcard = movedCardsHistory.pop();
        List<Flashcard> allFlashcards = flashcardProvider.getAllFlashcards();
        allFlashcards.remove(flashcard);
        allFlashcards.add(0, flashcard);

        Set<Flashcard> modified = flashcardProvider.fixSortOrder(allFlashcards);
        flashcardProvider.update(modified);
    }

    public void add(Flashcard flashcard) {
        add(Lists.newArrayList(flashcard));
    }

    public void add(List<Flashcard> flashcards) {
        List<Flashcard> allFlashcards = flashcardProvider.getAllFlashcards();
        allFlashcards.addAll(flashcards);
        flashcardProvider.save(flashcards);
        Set<Flashcard> modified = flashcardProvider.fixSortOrder(allFlashcards);
        flashcardProvider.update(modified);
    }

    public List<Flashcard> getAllFlashcardsInAlphabetical() {
        List<Flashcard> data = Lists.newArrayList(getAllFlashcards());
        Collections.sort(data, new Comparator<Flashcard>() {
            @Override
            public int compare(Flashcard lhs, Flashcard rhs) {
                return GermanGender.strip(lhs.getWordFrom())
                        .compareToIgnoreCase(
                                GermanGender.strip(rhs.getWordFrom()));
            }
        });
        return data;
    }

    public String generateCjkIdeograph(final String wordFrom, final String wordTo) {

        String cjk = flashcardProvider.generateUniqueCjk(wordFrom, wordTo);
        return cjk;
    }

    public List<Flashcard> getFlashcards(int offset, int size) {
        return flashcardProvider.getPage(offset, size);
    }
}
