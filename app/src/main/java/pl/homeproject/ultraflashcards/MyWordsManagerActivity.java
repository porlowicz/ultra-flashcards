package pl.homeproject.ultraflashcards;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.google.common.collect.Lists;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.List;

import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import pl.homeproject.ultraflashcards.wire.UltraFlashcardsViewWire;

public class MyWordsManagerActivity extends Activity {

    private UltraFlashcardsViewWire viewWire;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_words_manager);

        viewWire = new UltraFlashcardsViewWire(this);

        View rootView = findViewById(android.R.id.content);

        viewWire.wireAddWordsButton(rootView);
        viewWire.wireScanQRCodeButton(rootView);
        viewWire.wireEditButtons(rootView);
        viewWire.refreshWordsOverviewScrollView(rootView);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        final IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult == null) {
            return;
        }
        final String contents = scanResult.getContents();
        if (contents == null) {
            return;
        }
        String[] contentsSplit = contents.split("\\n");
        List<Flashcard> flashcards = Lists.newArrayList();
        for (String line : contentsSplit) {
            String[] words = line.split("-");
            Flashcard flashcard = Flashcard.create(words[0].trim(), words[1].trim());
            flashcards.add(flashcard);
        }
        viewWire.add(flashcards);
    }
}
