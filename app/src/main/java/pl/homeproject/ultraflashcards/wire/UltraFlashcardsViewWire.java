package pl.homeproject.ultraflashcards.wire;

import java.util.List;
import java.util.Set;

import pl.homeproject.ultraflashcards.R;
import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import pl.homeproject.ultraflashcards.flashcards.FlashcardDealer;
import pl.homeproject.ultraflashcards.popupWindow.HintPopupWindow;
import pl.homeproject.ultraflashcards.view.MarkAllButton;
import pl.homeproject.ultraflashcards.view.WordsOverviewRow;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.zxing.integration.android.IntentIntegrator;

public class UltraFlashcardsViewWire {

    private final FlashcardDealer flashcardDealer;
    private Context context;

    public UltraFlashcardsViewWire(Context context) {
        this.context = context;
        this.flashcardDealer = FlashcardDealer.getInstance(context);
    }

    public void wireAddWordsButton(final View fragmentView) {
        final EditText editWordFrom = (EditText) fragmentView
                .findViewById(R.id.edit_word_from);
        final EditText editWordTo = (EditText) fragmentView
                .findViewById(R.id.edit_word_to);

        View addWordsButton = fragmentView.findViewById(R.id.add_words_button);
        addWordsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String wordFrom = editWordFrom.getText().toString();
                String wordTo = editWordTo.getText().toString();
                if (wordFrom.isEmpty() && wordTo.isEmpty()) {
                    return;
                }

                Flashcard flashcard;
                if (markedWordsOverviewRows.size() == 1) {
                    WordsOverviewRow wordsOverviewRow = markedWordsOverviewRows
                            .get(0);
                    flashcard = wordsOverviewRow.getFlashcard();
                    flashcard.setWordFrom(wordFrom);
                    flashcard.setWordTo(wordTo);

                    wordsOverviewRow.restoreBackgroundResource();
                    markedWordsOverviewRows.clear();

                    flashcardDealer.update(Sets.newHashSet(flashcard));

                    wordUnmarkedView(fragmentView);

                } else {
                    flashcard = Flashcard.create(wordFrom, wordTo);
                    flashcardDealer.add(flashcard);
                }
                editWordFrom.setText("");
                editWordTo.setText("");
                InputMethodManager imm = (InputMethodManager) fragmentView
                        .getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editWordFrom.getWindowToken(), 0);

                refreshWordsOverviewScrollView(fragmentView);
                fillLearningBoard(((Activity) context)
                        .findViewById(R.id.fragment_1));
            }
        });
    }


    public void add(List<Flashcard> flashcards) {
        flashcardDealer.add(flashcards);
        refreshWordsOverviewScrollView(((Activity) context).findViewById(R.id.myWordActivityLayout));
    }

    public void refreshWordsOverviewScrollView(View view) {
        List<Flashcard> allFlashcards = flashcardDealer.getAllFlashcardsInAlphabetical();

        TextView wordCount = (TextView) view.findViewById(R.id.word_count);
        wordCount.setText(String.format("%s", allFlashcards.size()));

        ViewGroup wordsListLayout = (ViewGroup) view
                .findViewById(R.id.words_list_layout);
        wordsListLayout.removeAllViews();
        int i = 0;
        for (Flashcard f : allFlashcards) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            WordsOverviewRow inflate = (WordsOverviewRow) inflater.inflate(
                    R.layout.part_words_overview_row, null);
            inflate.setFlashcard(f);
            updateOverviewTextFields(view, inflate, f, i++);

            wordsListLayout.addView(inflate);
        }
    }

    private final List<WordsOverviewRow> markedWordsOverviewRows = Lists
            .newArrayList();

    private void updateOverviewTextFields(final View fragmentView,
                                          View inflate, Flashcard f, int index) {

        OnClickListener l = new OnClickListener() {
            @Override
            public void onClick(View v) {
                View parent = (View) v.getParent();
                if (parent instanceof WordsOverviewRow) {
                    WordsOverviewRow overviewRow = (WordsOverviewRow) parent;
                    if (overviewRow.isMarked()) {
                        overviewRow.restoreBackgroundResource();
                        markedWordsOverviewRows.remove(overviewRow);
                    } else {
                        overviewRow.setMarkedRowBackground();
                        markedWordsOverviewRows.add(overviewRow);
                    }
                }
                EditText editWordFrom = (EditText) fragmentView
                        .findViewById(R.id.edit_word_from);
                EditText editWordTo = (EditText) fragmentView
                        .findViewById(R.id.edit_word_to);
                MarkAllButton unmarkButton = (MarkAllButton) fragmentView
                        .findViewById(R.id.unmark_button);

                if (markedWordsOverviewRows.size() == 1) {
                    WordsOverviewRow wordsOverviewRow = markedWordsOverviewRows
                            .get(0);
                    Flashcard flashcard = wordsOverviewRow.getFlashcard();
                    editWordFrom.setText(flashcard.getWordFrom());
                    editWordTo.setText(flashcard.getWordTo());

                    wordMarkedView(fragmentView);

                    unmarkButton.setMarkAll(false);

                } else if (markedWordsOverviewRows.size() > 1) {
                    editWordFrom.setText("");
                    editWordTo.setText("");
                    wordMarkedView(fragmentView);
                    unmarkButton.setMarkAll(false);
                } else {
                    editWordFrom.setText("");
                    editWordTo.setText("");
                    unmarkButton.setMarkAll(true);
                    wordUnmarkedView(fragmentView);
                }
            }
        };

        TextView overviewWordFrom = (TextView) inflate
                .findViewById(R.id.overview_word_from);
        if (overviewWordFrom != null) {
            overviewWordFrom.setText(f.getWordFrom());
        }
        overviewWordFrom.setOnClickListener(l);

        TextView overviewWordTo = (TextView) inflate
                .findViewById(R.id.overview_word_to);
        if (overviewWordTo != null) {
            overviewWordTo.setText(f.getWordTo());
        }
        overviewWordTo.setOnClickListener(l);

        if (index % 2 == 1) {
            inflate.setBackgroundResource(R.color.zebra_white);
        } else {
            inflate.setBackgroundResource(R.color.zebra_black);
        }
    }


    public static void updateLearnTextFields(final WordsOverviewRow inflate) {

        final Flashcard f = inflate.getFlashcard();
        final TextView learnWordFrom = (TextView) inflate
                .findViewById(R.id.learn_word_from);
        if (learnWordFrom != null) {
            learnWordFrom.setText(f.getWordFrom());
        }
        int wordFromGenderColor = f.getWordFromBackgroundColor();
        learnWordFrom.setBackgroundResource(wordFromGenderColor);

        final View learnWordToClue = inflate
                .findViewById(R.id.learn_word_to_clue);
        learnWordToClue.setVisibility(View.VISIBLE);

        final TextView learnWordTo = (TextView) inflate
                .findViewById(R.id.learn_word_to);
        if (learnWordTo != null) {
            learnWordTo.setText("");
            learnWordTo.setOnClickListener(new OnClickListener() {
                private boolean visible = false;

                @Override
                public void onClick(View v) {
                    if (visible) {
                        learnWordTo.setText("");
                        learnWordToClue.setVisibility(View.VISIBLE);
                    } else {
                        learnWordTo.setText(f.getWordTo());
                        learnWordToClue.setVisibility(View.GONE);
                    }
                    visible = !visible;
                }
            });
        }
        int wordToGenderColor = f.getWordToBackgroundColor();
        learnWordTo.setBackgroundResource(wordToGenderColor);

        learnWordToClue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                HintPopupWindow popupWindow = new HintPopupWindow(v
                        .getContext(), f);
                popupWindow.show();

            }
        });
        ;
    }

    public void fillLearningBoard(View rootView) {
        fillLearningBoard(rootView, 0);
    }

    public void fillLearningBoard(View rootView, int pageOffset) {
        if (rootView == null) {
            return;
        }
        ViewGroup learnList = (ViewGroup) rootView
                .findViewById(R.id.learn_list);

        List<Flashcard> flashcards;
        if (pageOffset < 0) {
            flashcards = flashcardDealer.getPreviousPage();
        } else if (pageOffset > 0) {
            flashcards = flashcardDealer.getNextPage();
        } else {
            flashcards = flashcardDealer.getCurrentPage();
        }
        TextView paginationStatus = (TextView) rootView.findViewById(R.id.paginationStatus);
        paginationStatus.setText(String.valueOf(flashcardDealer.getPaginationCurrent() + "/" + flashcardDealer.getPaginationMax()));
        if (flashcards.isEmpty()) {
            return;
        }
        if (learnList != null) {
            learnList.removeAllViews();
            for (Flashcard f : flashcards) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                WordsOverviewRow inflate = (WordsOverviewRow) inflater.inflate(
                        R.layout.part_learning_row, null);
                inflate.setFlashcard(f);
                updateLearnTextFields(inflate);
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.0f);
                inflate.setLayoutParams(param);

                learnList.addView(inflate);
            }
        }
    }

    public void wirePagingButtons(final View rootView) {
        View previousWordsButton = rootView
                .findViewById(R.id.previous_words_button);
        View nextWordsButton = rootView.findViewById(R.id.next_words_button);

        if (previousWordsButton != null) {
            previousWordsButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    fillLearningBoard(rootView, -1);
                }
            });
            nextWordsButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    fillLearningBoard(rootView, 1);
                }
            });
        }
    }

    public void wireScanQRCodeButton(View rootView) {

        View scanQrCodeButton = rootView.findViewById(R.id.scan_qr_code_button);
        scanQrCodeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(
                        (Activity) context);
                integrator.initiateScan();
            }
        });
    }

    public void wireEditButtons(final View rootView) {

        View swapButton = rootView.findViewById(R.id.swap_button);

        swapButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<Flashcard> flashcards = Sets.newHashSet();
                for (WordsOverviewRow overviewRow : markedWordsOverviewRows) {
                    Flashcard flashcard = overviewRow.getFlashcard();
                    String wordFrom = flashcard.getWordFrom();
                    flashcard.setWordFrom(flashcard.getWordTo());
                    flashcard.setWordTo(wordFrom);
                    overviewRow.restoreBackgroundResource();
                    flashcards.add(flashcard);
                }
                flashcardDealer.update(flashcards);

                markedWordsOverviewRows.clear();
                MarkAllButton unmarkButton = (MarkAllButton) rootView
                        .findViewById(R.id.unmark_button);
                unmarkButton.setMarkAll(true);
                rootView.findViewById(R.id.overview_edit_buttons_layout)
                        .setVisibility(View.GONE);
                rootView.findViewById(R.id.word_count).setVisibility(View.VISIBLE);

                ((TextView) rootView.findViewById(R.id.edit_word_from)).setText("");
                ((TextView) rootView.findViewById(R.id.edit_word_to)).setText("");

                refreshWordsOverviewScrollView(rootView);
                fillLearningBoard(((Activity) context).findViewById(R.id.fragment_1));
            }
        });

        final MarkAllButton unmarkButton = (MarkAllButton) rootView
                .findViewById(R.id.unmark_button);
        unmarkButton.setMarkAll(true);
        unmarkButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (unmarkButton.isMarkAll()) {

                    unmark();
                    ViewGroup wordsListLayout = (ViewGroup) rootView
                            .findViewById(R.id.words_list_layout);
                    for (int i = 0; i < wordsListLayout.getChildCount(); i++) {
                        WordsOverviewRow overviewRow = (WordsOverviewRow) wordsListLayout
                                .getChildAt(i);
                        overviewRow.setMarkedRowBackground();
                        markedWordsOverviewRows.add(overviewRow);
                    }
                    wordMarkedView(rootView);
                } else {
                    unmark();
                    wordUnmarkedView(rootView);
                }
                ((TextView) rootView.findViewById(R.id.edit_word_from)).setText("");
                ((TextView) rootView.findViewById(R.id.edit_word_to)).setText("");

                unmarkButton.setMarkAll(!unmarkButton.isMarkAll());
            }

            private void unmark() {
                for (WordsOverviewRow overviewRow : markedWordsOverviewRows) {
                    overviewRow.restoreBackgroundResource();
                }
                markedWordsOverviewRows.clear();
            }
        });

        View removeButton = rootView.findViewById(R.id.remove_button);
        removeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Flashcard> flashcards = Lists.newArrayList();
                for (WordsOverviewRow overviewRow : markedWordsOverviewRows) {
                    Flashcard flashcard = overviewRow.getFlashcard();
                    flashcards.add(flashcard);
                }
                flashcardDealer.remove(flashcards);

                markedWordsOverviewRows.clear();

                EditText editWordFrom = (EditText) rootView
                        .findViewById(R.id.edit_word_from);
                EditText editWordTo = (EditText) rootView
                        .findViewById(R.id.edit_word_to);
                editWordFrom.setText("");
                editWordTo.setText("");
                InputMethodManager imm = (InputMethodManager) rootView
                        .getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editWordFrom.getWindowToken(), 0);

                wordUnmarkedView(rootView);

                MarkAllButton unmarkButton = (MarkAllButton) rootView
                        .findViewById(R.id.unmark_button);
                unmarkButton.setMarkAll(true);
                refreshWordsOverviewScrollView(rootView);
                fillLearningBoard(((Activity) context)
                        .findViewById(R.id.fragment_1));
            }
        });

    }

    private void wordUnmarkedView(View fragmentView) {
        View overviewEditButtonsLayout = fragmentView
                .findViewById(R.id.overview_edit_buttons_layout);
        overviewEditButtonsLayout.setVisibility(View.GONE);
        fragmentView.findViewById(R.id.swap_button).setVisibility(View.GONE);

        View scanQrCodeButton = fragmentView
                .findViewById(R.id.scan_qr_code_button);
        scanQrCodeButton.setVisibility(View.VISIBLE);
        fragmentView.findViewById(R.id.word_count).setVisibility(View.VISIBLE);
    }

    private void wordMarkedView(View fragmentView) {
        View overviewEditButtonsLayout = fragmentView
                .findViewById(R.id.overview_edit_buttons_layout);
        overviewEditButtonsLayout.setVisibility(View.VISIBLE);
        fragmentView.findViewById(R.id.swap_button).setVisibility(View.VISIBLE);

        View scanQrCodeButton = fragmentView
                .findViewById(R.id.scan_qr_code_button);
        scanQrCodeButton.setVisibility(View.GONE);
        fragmentView.findViewById(R.id.word_count).setVisibility(View.GONE);
    }

}
