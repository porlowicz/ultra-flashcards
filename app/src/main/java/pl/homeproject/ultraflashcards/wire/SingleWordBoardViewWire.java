package pl.homeproject.ultraflashcards.wire;


import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.util.Random;

import pl.homeproject.ultraflashcards.R;
import pl.homeproject.ultraflashcards.flashcards.Flashcard;
import pl.homeproject.ultraflashcards.flashcards.FlashcardDealer;
import pl.homeproject.ultraflashcards.popupWindow.HintPopupWindow;
import pl.homeproject.ultraflashcards.view.swipe.SwipeDirection;

public class SingleWordBoardViewWire {

    private static final Random r = new Random();

    private static final String CARD_BACK = "";

    private final FlashcardDealer flashcardDealer;
    private Context context;

    public SingleWordBoardViewWire(Context context) {
        this.context = context;
        this.flashcardDealer = FlashcardDealer.getInstance(context);

    }

    public void fillSingleWordLearningBoard(final View rootView) {

        if (rootView == null) {
            return;
        }

        TextView singleFromTextView = (TextView) rootView.findViewById(R.id.singleFromTextView);
        TextView singleToTextView = (TextView) rootView.findViewById(R.id.singleToTextView);
        final TextView singleNextTextView = (TextView) rootView.findViewById(R.id.singleNextTextView);
        final TextView singlePrevTextView = (TextView) rootView.findViewById(R.id.singlePrevTextView);

        TextView symbolTextView = (TextView) rootView.findViewById(R.id.symbolTextView);

        TextView singleHint = (TextView) rootView.findViewById(R.id.singleHint);

        final Flashcard flashcard = flashcardDealer.getPage(0).get(0);

        singleHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HintPopupWindow popupWindow = new HintPopupWindow(v
                        .getContext(), flashcard);
                popupWindow.show();
            }
        });

        if (Strings.isNullOrEmpty(flashcard.getCjkIdeograph())) {
            String cjkIdeograph = flashcardDealer.generateCjkIdeograph(flashcard.getWordFrom(), flashcard.getWordTo());
            flashcard.setCjkIdeograph(cjkIdeograph);
        }
        symbolTextView.setText(flashcard.getCjkIdeograph());

        singleFromTextView.setText(flashcard.getWordFrom());
        singleToTextView.setText(CARD_BACK);

        singleToTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                if (CARD_BACK.equals(tv.getText().toString())) {
                    tv.setText(flashcard.getWordTo());
                } else {
                    tv.setText(CARD_BACK);
                }
            }
        });

        singleNextTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashcardDealer.move(flashcard, SwipeDirection.RIGHT);
                if (r.nextInt(4) % 4 == 0) {
                    String wordFrom = flashcard.getWordFrom();
                    flashcard.setWordFrom(flashcard.getWordTo());
                    flashcard.setWordTo(wordFrom);
                }
                fillSingleWordLearningBoard(rootView);
            }
        });

        singlePrevTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashcardDealer.undo();
                fillSingleWordLearningBoard(rootView);
            }
        });
    }
}
