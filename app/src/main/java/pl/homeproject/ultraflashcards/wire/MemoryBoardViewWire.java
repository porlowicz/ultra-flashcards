package pl.homeproject.ultraflashcards.wire;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pl.homeproject.ultraflashcards.R;
import pl.homeproject.ultraflashcards.flashcards.FlashcardDealer;
import pl.homeproject.ultraflashcards.learning.mode.memory.MemoryBoardManager;
import pl.homeproject.ultraflashcards.view.MemoryTextView;

public class MemoryBoardViewWire {

    private final Context context;
    private final FlashcardDealer flashcardDealer;

    public MemoryBoardViewWire(Context context) {
        this.context = context;
        this.flashcardDealer = FlashcardDealer.getInstance(context);
    }

    public void initBoard(View rootView) {
        boolean isPrepared = MemoryBoardManager.getInstance(rootView.getContext()).prepareBoard();

        RelativeLayout memGameboardLayout = (RelativeLayout) rootView.findViewById(R.id.mem_gameboard_layout);
        memGameboardLayout.removeAllViews();

        if(!isPrepared){
            return;
        }

        for (int r = 0; r < 5; r++) {
            for (int c = 0; c < 3; c++) {
                TextView textView = new MemoryTextView(rootView.getContext(), r, c);
                memGameboardLayout.addView(textView);
            }
        }
    }

    public void wirePrevious(final View root) {

        View memoryPrev = root.findViewById(R.id.mem_prev);

        memoryPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MemoryBoardManager.getInstance(root.getContext()).previousOffset();
                initBoard(root);
            }
        });

    }

    public void wireRestart(final View root) {

        View memoryReset = root.findViewById(R.id.memory_reset);
        memoryReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initBoard(root);

            }
        });

    }

    public void wireNext(final View root) {
        View memoryNext = root.findViewById(R.id.mem_next);

        memoryNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MemoryBoardManager.getInstance(root.getContext()).nextOffset();
                initBoard(root);
            }
        });

    }

}
