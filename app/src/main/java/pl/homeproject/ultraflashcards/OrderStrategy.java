package pl.homeproject.ultraflashcards;

import java.util.List;

import pl.homeproject.ultraflashcards.flashcards.Flashcard;

public abstract class OrderStrategy {

	public abstract List<Flashcard> transform(List<Flashcard> flashcards);
}
